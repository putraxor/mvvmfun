package com.gitlab.putraxor.mvvmfun.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.gitlab.putraxor.mvvmfun.R
import com.gitlab.putraxor.mvvmfun.databinding.LoginBinding

/**
 * MVVMFun
 * Created by putraxor on 23/12/17.
 */
class LoginFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, bundle: Bundle?): View? {
        val binding = DataBindingUtil.inflate<LoginBinding>(inflater, R.layout.login, container, false)
        return binding.root
    }
}