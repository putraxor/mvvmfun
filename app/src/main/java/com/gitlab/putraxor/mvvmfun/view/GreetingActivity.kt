package com.gitlab.putraxor.mvvmfun.view

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gitlab.putraxor.mvvmfun.R
import com.gitlab.putraxor.mvvmfun.databinding.GreetingBinding
import com.gitlab.putraxor.mvvmfun.viewmodel.GreetingViewModel

/**
 * MVVMFun
 * Created by putraxor on 23/12/17.
 */
class GreetingActivity : AppCompatActivity(){
    private val binding by lazy {
        DataBindingUtil.setContentView<GreetingBinding>(this, R.layout.greeting)
    }

    private val viewModel = GreetingViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.vm = viewModel
    }
}