package com.gitlab.putraxor.mvvmfun.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.view.View
import com.gitlab.putraxor.mvvmfun.BR
import com.gitlab.putraxor.mvvmfun.model.User
import com.gitlab.putraxor.mvvmfun.model.Validation

/**
 * Android Developer Medan #Meetup3
 * Created by putraxor on 22/12/17.
 */
class LoginViewModel : BaseObservable() {

    private var model = User()

    //Property username
    @get:Bindable
    var username = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.username)
            model = model.copy(username = value)
        }

    //Property password
    @get:Bindable
    var password = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.password)
            model = model.copy(password = value)
        }

    var onYudi: ((Validation) -> Unit)? = null
    var onReset: (() -> Unit)? = null

    /**
     * Binding listener
     */
    fun onClickLogin(view: View) {
        val result = model.validate()
        onYudi?.invoke(result)
    }
}