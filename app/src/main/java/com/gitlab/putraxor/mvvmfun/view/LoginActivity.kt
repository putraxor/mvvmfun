package com.gitlab.putraxor.mvvmfun.view

import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.gitlab.putraxor.mvvmfun.R
import com.gitlab.putraxor.mvvmfun.databinding.LoginBinding
import com.gitlab.putraxor.mvvmfun.viewmodel.LoginViewModel
import org.jetbrains.anko.toast

/**
 * LeetFi
 * Created by putraxor on 22/12/17.
 */
class LoginActivity : AppCompatActivity() {

    private val binding by lazy {
        DataBindingUtil.setContentView<LoginBinding>(this, R.layout.login)
    }

    private val viewModel = LoginViewModel().apply {
        onYudi = {
            toast("Validation Result ${it.message}")
            val greeting = Intent(applicationContext, GreetingActivity::class.java)
            startActivity(greeting)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.vm = viewModel
    }

}