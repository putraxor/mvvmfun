package com.gitlab.putraxor.mvvmfun.model

/**
 * MVVMFun
 * Created by putraxor on 23/12/17.
 */
data class Validation(val valid: Boolean, val message: String)