package com.gitlab.putraxor.mvvmfun.model

/**
 * MVVMFun
 * Created by putraxor on 23/12/17.
 */
data class Greeting (val name: String="")