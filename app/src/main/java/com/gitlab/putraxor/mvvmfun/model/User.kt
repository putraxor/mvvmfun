package com.gitlab.putraxor.mvvmfun.model

/**
 * Android Developer Medan #Meetup3
 * Created by putraxor on 22/12/17.
 */

data class User(val username: String="", val password: String="") {
    fun validate() = when {
        username.isEmpty() -> Validation(false, "Empty username")
        password.isEmpty() -> Validation(false, "Empty password")
        else -> Validation(true, "Valid")
    }
}


