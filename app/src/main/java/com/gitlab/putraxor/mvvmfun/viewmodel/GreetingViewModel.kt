package com.gitlab.putraxor.mvvmfun.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableField
import com.gitlab.putraxor.mvvmfun.BR
import com.gitlab.putraxor.mvvmfun.model.Greeting

/**
 * MVVMFun
 * Created by putraxor on 23/12/17.
 */
class GreetingViewModel : BaseObservable() {
    var model = Greeting()

    //Property name
    @get:Bindable
    var name = ""
        set(value) {
            field = value
            notifyPropertyChanged(BR.name)
            model = model.copy(name = value)
            greeting.set("Hello world $name!")
        }
    val greeting = ObservableField<String>("")
}